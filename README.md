# Cactus To LIGO
Convert the output format from cactus to LIGO so that it can be directly used by LIGO Algorithm Library (LAL).

# Prerequisite
- **python 3.5+**
    - numpy
    - h5py
    - [romSpline](https://bitbucket.org/chadgalley/romspline) (no need to install since it's already packed in this repository for python3 compatibility)
- **Mathematica**
    - [SimulationTools](http://www.simulationtools.org/)
    - h5mma

## Installing Prerequisite

### Python 3.5+
Make sure `numpy` and `h5py` packages are installed in the python environment that will be running.

### Mathematica

#### `SimulationTools` and `h5mma`
The official website provides information how to [download](http://simulationtools.org/download.shtml) and install `SimulationTools` and `h5mma`. Below is a summary of instructions:

1. Locate the path to Mathematica Applications. To find out the path, you can run the following command in Mathematica:

    ```
FileNameJoin[{$UserBaseDirectory, "Applications"}]
    ```

    *By default Mathematica will install the Applications Path to `$HOME/Library/Mathematica/Applications` (macOS) or `$HOME/.Mathematica/Applications` (Linux).*

2. Change directory to the Mathematica Applications path. For example, you can do the following if on Linux with default Mathematica installation:

    ```
cd $HOME/.Mathematica/Applications
    ```

    *Note: this directory might not be created after fresh installation. In that case, you can manually create this directory or just open Mathematica at least once to make sure the path exists.*

3. Download and install `SimulationTools` and `h5mma`

    ```bash
# Check out SimulationTools
git clone --recursive https://bitbucket.org/simulationtools/SimulationTools.git
# Download h5mma
curl -L --remote-name "https://bitbucket.org/simulationtools/h5mma/downloads/h5mma-1.2.1.tar.gz"
tar -xzvf h5mma-1.2.1.tar.gz
```

4. Check out the NCSA-converter
    ```bash
git clone https://git.ncsa.illinois.edu/weiren2/NCSA-Converter.git
cd NCSA-Converter
    ```

# Running the Script
The NCSA-converter is already encapsulated into a **CactusConverter** class in **CactusConverter.py**. To use it, simply construct an **CactusConverter** object with correct parameters (most importantly the input and output path). Check comments in **CactusConverter.py** for more details on the functions.

The python script **convertToLIGO.py** shows an example that you can use to convert to LIGO format and extract relevant information (such as amplitude, phase...) from Cactus output. It also added support for command line inputs. Also see comments in **convertToLIGO.py** for some example uses.

Usage:
```bash
./convertToLIGO.py [group name] [simDir] [outdir] [simulation names]
```
## Some concrete examples
- Run from command line:
```bash
./convertToLIGO.py /path/to/simulations /path/to/output E0001_N32 E0001_N36 E0001_N40
```

- Or use it in a shell script or PBS job script
```bash
# Define input and output path below
INPUT_SIMULATION_PATH=/path/to/simulations
OUTPUT_PATH=/path/to/output
# Make sure qsub script is in the same directory with CactusToLIGO
cd $PBS_O_WORKDIR
# Remember to change the list of simulations at the end.
./convertToLIGO.py $INPUT_SIMULATION_PATH $OUTPUT_PATH E0001_N32 E0001_N36 E0001_N40
```

# Author information
Authors: Wei Ren (*weiren2 "at" illinois "dot" edu*), Eliu Huerta (*elihu "at" illinois "dot" edu*), Roland Haas (*rhaas "at" illinois "dot" edu*), Ian Hinder (*ian.hinder "at" aei "dot" mpg "dot"de*)
Copyright (C) 2016 Trustees of the University of Illinois
Included Romspline
Copyright (C) 2016 Trustees of the University of Illinois
Copyright (C) 2015 Chad Galley (*crgalley "at" tapir "dot" caltech "dot" edu*).

# Issues
Please contact [ncsarelativitygroup@ncsa.illinois.edu](mailto:ncsarelativitygroup@ncsa.illinois.edu) for any bug or improvement.

## Miscellaneous
- [ ] Remove the hardcoded part when filling in attributes.
- [x] ~~Make python script use command line argument.~~
- [x] romSpline cloned from [Bitbucket](https://bitbucket.org/chadgalley/romspline) (**git hash: [ea8b216](https://bitbucket.org/chadgalley/romspline/commits/ea8b21624ecb71f3358944bd724d4db03431ac75)**)
