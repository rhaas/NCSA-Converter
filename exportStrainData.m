#!/usr/bin/env wolframscript
# A Mathematica script to export data in [2,2] mode.

Get["SimulationTools`"];

# Parse command line argument
SimulationTools`$SimulationPath={$CommandLine[[-1]]};
sim=$CommandLine[[-3]];
outDir=$CommandLine[[-2]];
r0 = ToExpression[$CommandLine[[-4]]];
\[Omega]0=ToExpression[$CommandLine[[-5]]];

# Export data
MADM = ReadADMMass[sim];
\[Psi]4[2,2]=ReadPsi4[sim,2,2,r0];
r\[Psi]4[2,2]=Shifted[r0 \[Psi]4[2,2],-RadialToTortoise[r0,MADM]];
dest = outDir<>"/"<>sim<>"/";
If[!DirectoryQ[dest],CreateDirectory[dest]];
Export[dest<>sim<>"_omega.dat", -Frequency[r\[Psi]4[2, 2]]];
Export[dest<>sim<>"_phase.dat",-Phase[r\[Psi]4[2,2]]];
rh[2,2]=Psi4ToStrain[r\[Psi]4[2,2],\[Omega]0];
Export[dest<>sim<>"_real.dat",Re[rh[2,2]]];
Export[dest<>sim<>"_imag.dat", Im[rh[2,2]]];
Export[dest<>sim<>"_amp.dat", Abs[r\[Psi]4[2,2]]]

# Quit
Print["Export strain data of " <> sim <> " done."];
LinkClose[#] & /@ Links[];
CloseKernels[];
Quit[];
