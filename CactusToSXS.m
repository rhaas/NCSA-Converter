#!/usr/bin/env wolframscript
# Mathematica script to export Cactus BBH simulation to SXS format.

Get["SimulationTools`"];

# Parse command line argument
SimulationTools`$SimulationPath={$CommandLine[[-1]]};
sim=SimulationNames[$CommandLine[[-3]]];
outDir=$CommandLine[[-2]];

# Export hdf5 and metadata file
ExportSXSSimulation[sim[[1]],FileNameJoin[{outDir,sim[[1]]}]];

# Quit
LinkClose[#] & /@ Links[];
CloseKernels[];
Quit[];
