#!/usr/bin/env python3

import argparse
import sys
import os
from CactusConverter import CactusConverter

def main(simulationPath, outputPath, simulationList):
    converter = CactusConverter(simulationPath, outputPath, simulationList)

    # SXS files are generated in outputPath.
    converter.toSXS()

    # Set to False if you can make sure SXS is already generated.
    # (By default toLIGO() fuction will run toSXS anyway)
    # (To avoid duplicating the work, we can set exportSXS flag to False.)
    converter.toLIGO(exportSXS = False)

    # exportStrainData() function exports amplitudes (real and imaginary part),
    # phase and frequency raw data in separate files (.dat).
    converter.exportStrainData()

if __name__ == '__main__':
    if(len(sys.argv) < 4):
        sys.stderr.write("usage: %s [group name] [simDir] [outdir] [simulation names]\n" % sys.argv[0])
        sys.exit(1)
    simDir = sys.argv[1]
    outDir = sys.argv[2]
    simNames = sys.argv[3:]

    if not os.path.exists(simDir):
        sys.stderr.write("%s: File %s not found\n" % (sys.argv[0], simDir))
        sys.exit(1)
    simDir = os.path.abspath(simDir)

    if not os.path.exists(outDir):
        sys.stderr.write("%s: File %s not found\n" % (sys.argv[0], outDir))
        sys.exit(1)
    outDir = os.path.abspath(outDir)

    print(simDir)
    print(outDir)
    print(simNames)

    main(simDir, outDir, simNames)
